# etg-docker: ekleinod/mkdocs-material

## Quick reference

- **Maintained by:** [ekleinod](https://gitlab.com/etg-docker/mkdocs-material/)
- **Docker hub:** [ekleinod/mkdocs-material](https://hub.docker.com/r/ekleinod/mkdocs-material)
- **Where to get help:** [gitlab](https://gitlab.com/etg-docker/mkdocs-material/), [issue tracker](https://gitlab.com/etg-docker/mkdocs-material/-/issues)


## Supported tags

- `1.4.0`, `1.4.0-9.5.22`, `latest`
- `1.3.0`, `1.3.0-9.5.10`
- `1.2.0`, `1.2.0-8.2.16`
- `1.1.0`, `1.1.0-8.1.3`
- `1.0.2`, `1.0.2-8.1.2`
- `1.0.1`, `1.0.1-8.1.2`


## What is this image?

This is a docker image for [mkdocs-material](https://squidfunk.github.io/mkdocs-material/).
The image has the name `ekleinod/mkdocs-material`.
It is based on the excellent [official mkdocs-material image](https://hub.docker.com/r/squidfunk/mkdocs-material) and extends it with the plugins/extensions needed for my projects.

The additions are:

- weasyprint
- mkdocs-pdf-export-plugin
- mkdocs-static-i18n

For the complete changelog, see [changelog.md](https://gitlab.com/etg-docker/mkdocs-material/-/blob/main/changelog.md)


## How to use the image

You can use the image as follows:

~~~ bash
docker run --rm --interactive --tty --name <containername> --user "$(id -u):$(id -g)" --volume "${PWD}":/docs ekleinod/mkdocs-material <command>
~~~

Please note, that `/docs` is the work directory of the image.

For a simple test showing the mkdocs-material version, run

~~~ bash
$ docker run --rm --name mkdocs-material ekleinod/mkdocs-material --version
~~~

You can see examples at work in the test folder.
Clone the repository or download the folder.
Call them as follows:

~~~ bash
$ cd test
$ ./test_help.sh
$ ./test_hello-world.sh
$ ./test_version.sh
~~~


## Releases

The latest release will always be available with:

`ekleinod/mkdocs-material:latest`

There are two naming schemes:

1. `ekleinod/mkdocs-material:<internal>-<mkdocs-material>`

	Example: `ekleinod/mkdocs-material:1.0.1-8.1.2`

2. internal version number `ekleinod/mkdocs-material:<major>.<minor>.<patch>`

	Example: `ekleinod/mkdocs-material:1.0.1`


## Build locally

In order to build the image locally, clone the repository and call

~~~ bash
$ cd image
$ ./build_image.sh
~~~

## Git-Repository

The branching model regards to the stable mainline model described in <https://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git/>.

This means, there is always a stable mainline, the main branch.
This branch ist always compileable and testable, both without errors.

Features are developed using feature branches.
Special feature branches are used (different from the mainline model) for finalizing releases.

Releases are created as branches of the mainline.
Additionally, each release is tagged, tags contain the patch and, if needed, additional identifiers, such as `rc1` or `beta`.

Patches are made in the according release branch.
Minor version changes get their own release branch.

## Copyright

Copyright 2021-2024 Ekkart Kleinod <ekleinod@edgesoft.de>

The program is distributed under the terms of the GNU General Public License, either version 3 of the License, or
any later version.

See COPYING for details.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
