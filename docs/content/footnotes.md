# Footnotes

!!! warning ""
	Use scarcely, as footnotes are a thing of books, not the web.

- [Footnotes](https://squidfunk.github.io/mkdocs-material/reference/footnotes/)

~~~ yaml title="mkdocs.yml"
markdown_extensions:
  - footnotes
~~~

=== "Result"

	This is a text[^1] with a footnote.
	[^1]: The footnote is of no real value at the moment.

=== "Markdown"

	~~~~ markdown
	This is a text[^1] with a footnote.
	[^1]: The footnote is of no real value at the moment.
	~~~~
