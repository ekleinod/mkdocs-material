# PDF-Export

PDF export ist built-in in the image via:

- [mkdocs-pdf-export-plugin](https://github.com/zhaoterryy/mkdocs-pdf-export-plugin)
- weasyprint

Use the *mkdocs-pdf-export-plugin* plugin in `mkdocs.yml`:

~~~ yaml title="mkdocs.yml"
plugins:
  - pdf-export
~~~

This generates a downloadable pdf file for each documentation page.

If you want to have a single pdf, use:

~~~ yaml title="mkdocs.yml"
plugins:
  - pdf-export:
      combined: true
~~~

For all other options, see the [plugin documentation](https://github.com/zhaoterryy/mkdocs-pdf-export-plugin)
