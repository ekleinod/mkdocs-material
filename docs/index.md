# Welcome

![Logo – a cat](images/logo.png){ align=right }

!!! tip ""
	Docker your documentations hassle free.

[ekleinod/mkdocs-material](https://hub.docker.com/r/ekleinod/mkdocs-material) is a docker image for [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/).

I created the image in order to have a documentation environment on different computers and operating systems.
This means the basic mkdocs and mkdocs-material functionality enriched with the plugins/extensions I need for my projects.

This documentation is not meant to be extensive but to provide me with the knowledge to adapt *mkdocs-material* to my needs.
And to provide a [link list](99_links.md "Go to the links!") to the really important pages.

Use [the template](https://gitlab.com/etg-docker/mkdocs-material/-/tree/main/template) :smile:
