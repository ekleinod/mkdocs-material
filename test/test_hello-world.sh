#!/bin/bash

source ../settings.sh

echo "Building hello world documentation."
echo

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--name $CONTAINERNAME \
	--volume "${PWD}:/docs" \
	$IMAGENAME \
		build --strict --verbose
