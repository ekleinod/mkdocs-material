# Diagrams

- [Diagrams](https://squidfunk.github.io/mkdocs-material/reference/diagrams/)
- [Mermaid](https://mermaid.js.org/syntax/flowchart.html)

~~~ yaml title="mkdocs.yml"
markdown_extensions:
  - pymdownx.superfences:
      custom_fences:
      - name: mermaid
        class: mermaid
        format: !!python/name:pymdownx.superfences.fence_code_format
~~~

=== "Result"

	~~~ mermaid
	graph LR

		a[Start] --> b[Use Material for MkDocs];
		b --> c[Happy];
	~~~

=== "Markdown"

	~~~~ markdown
	~~~ mermaid
	graph LR

		a[Start] --> b[Use Material for MkDocs];
		b --> c[Happy];
	~~~
	~~~~
