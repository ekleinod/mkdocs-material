# GitLab Pages

Normally mkdocs is used in combination with GitLab Pages.

1. enable pages in your *general* repository settings at *project features*
2. create a shell script for [building your documentation](../01_start.md#build-documentation)
3. create a `.gitlab-ci.yml` for the automated creation and deployment

GitLab Help:

- [overall help](https://docs.gitlab.com/ee/user/project/pages/)
- [domain names](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html)

Example `.gitlab-ci.yml`

~~~ yaml title=".gitlab-ci.yml"
stages:
  - deploy

pages:
  stage: deploy
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login $DOCKER_REGISTRY --username "$DOCKER_REGISTRY_USER" --password "$DOCKER_REGISTRY_PASSWORD"
  script:
    - sh build-docs.sh
  artifacts:
    paths:
      - public
  only:
    - main
    - /release\/*/
~~~
