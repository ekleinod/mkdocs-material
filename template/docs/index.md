# Welcome

![Logo – a cat](images/logo.png){ align=right }

!!! tip ""
	It works!

Really :smile:

~~~ mermaid
graph LR

	copy(copy template);
	adapt[adapt to your needs];
	fill[fill with documentation];
	happy(be happy);
	ice[eat ice cream];

	copy --> adapt --> fill --> happy;
	copy <--> ice;
	adapt <--> ice;
	fill <--> ice;
	happy <--> ice;
~~~
