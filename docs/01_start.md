# Starting

Just use the template or copy this documentation, adapt it and you're good to go :grin:

- [The Template](https://gitlab.com/etg-docker/mkdocs-material/-/tree/main/template)
- [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/)

## Write documentation with live server

- place your documentation in `docs`
- [serve-docs.sh](https://gitlab.com/etg-docker/mkdocs-material/-/blob/main/serve-docs.sh) or use directly

	~~~ shell
	docker run \
		--rm \
		--name mkdocs-material \
		--user "$(id -u):$(id -g)" \
		--interactive \
		--tty \
		--volume "${PWD}:/docs" \
		--publish 8000:8000 \
		ekleinod/mkdocs-material \
			$@
	~~~

- call <http://localhost:8000/>
- stop server with ++ctrl+c++

## Build documentation

- [build-docs.sh](https://gitlab.com/etg-docker/mkdocs-material/-/blob/main/build-docs.sh) or use directly

	~~~ shell
	docker run \
		--rm \
		--name mkdocs-material \
		--volume "${PWD}:/docs" \
		ekleinod/mkdocs-material \
			build --strict --verbose \
			$@
	~~~

- generated files are in `public`
- use this script for [pages generation](results/01_pages.md)
