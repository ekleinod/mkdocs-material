# Standalone HTML

Maybe you don't have a server or such to serve your documentation.
Generate HTML instead.

Use *offline* plugin in `mkdocs.yml`:

~~~ yaml title="mkdocs.yml"
plugins:
  - offline
~~~

See [documentation](https://squidfunk.github.io/mkdocs-material/plugins/offline/) for more explanation, especially controlling generation with environment variables.
