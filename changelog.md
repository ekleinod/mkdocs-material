# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog] and this project adheres to [Semantic Versioning].

## [Unreleased]

### Added

- documentation #7
- starter template


## [1.4.0-9.5.22] - 2024-05-15

### Added

- new test `test_bash.sh`

### Changed

- new mkdocs-material version `9.5.22`, mkdocs version `1.6.0` #6
- plain output of docker build


## [1.3.0-9.5.10] - 2024-02-22

### Changed

- new mkdocs-material version `9.5.10`, mkdocs version `1.5.3`


## [1.2.0-8.2.16] - 2022-06-01

### Added

- bash script for calling mkdocs

### Changed

- new mkdocs-material version 8.2.16


## [1.1.0-8.1.3] - 2021-12-27

### Added

- new plugin mkdocs-static-i18n

### Changed

- new mkdocs-material version 8.1.3


## [1.0.2-8.1.2] - 2021-12-17

### Removed

- temporary dependencies


## [1.0.1-8.1.2] - 2021-12-16

### Fixed

- CI/CD failed because of test scripts


## [1.0.0-8.1.2] - 2021-12-16

- initial version

### Added

- weasyprint
- mkdocs-pdf-export-plugin
- test files
- CI/CD for gitlab

[Unreleased]: https://gitlab.com/etg-docker/mkdocs-material/-/compare/1.4.0-9.5.22...HEAD
[1.4.0-9.5.22]: https://gitlab.com/etg-docker/mkdocs-material/-/compare/1.3.0-9.5.10...1.4.0-9.5.22
[1.3.0-9.5.10]: https://gitlab.com/etg-docker/mkdocs-material/-/compare/1.2.0-8.2.16...1.3.0-9.5.10
[1.2.0-8.2.16]: https://gitlab.com/etg-docker/mkdocs-material/-/compare/1.1.0-8.1.3...1.2.0-8.2.16
[1.1.0-8.1.3]: https://gitlab.com/etg-docker/mkdocs-material/-/compare/1.0.2-8.1.2...1.1.0-8.1.3
[1.0.2-8.1.2]: https://gitlab.com/etg-docker/mkdocs-material/-/compare/1.0.1-8.1.2...1.0.2-8.1.2
[1.0.1-8.1.2]: https://gitlab.com/etg-docker/mkdocs-material/-/compare/1.0.0-8.1.2...1.0.1-8.1.2
[1.0.0-8.1.2]: https://gitlab.com/etg-docker/mkdocs-material/-/tags/1.0.0-8.1.2

[Keep a Changelog]: https://keepachangelog.com/en/1.1.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
