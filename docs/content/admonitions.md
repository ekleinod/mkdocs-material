# Admonitions

- [Admonitions](https://squidfunk.github.io/mkdocs-material/reference/admonitions/)

~~~ yaml title="mkdocs.yml"
markdown_extensions:
  - admonition
~~~

=== "Result"

	!!! tip
		Docker your documentations hassle free.

=== "Markdown"

	~~~~ markdown
	!!! tip
		Docker your documentations hassle free.
	~~~~


## Own title

=== "Result"

	!!! tip "Do it!"
		Docker your documentations hassle free.


=== "Markdown"

	~~~~ markdown
	!!! tip "Do it!"
		Docker your documentations hassle free.
	~~~~


## No title

=== "Result"

	!!! tip ""
		Docker your documentations hassle free.


=== "Markdown"

	~~~~ markdown
	!!! tip ""
		Docker your documentations hassle free.
	~~~~

## Types

!!! note
	Docker your documentations hassle free.

!!! abstract
	Docker your documentations hassle free.

!!! info
	Docker your documentations hassle free.

!!! tip
	Docker your documentations hassle free.

!!! success
	Docker your documentations hassle free.

!!! question
	Docker your documentations hassle free.

!!! warning
	Docker your documentations hassle free.

!!! failure
	Docker your documentations hassle free.

!!! danger
	Docker your documentations hassle free.

!!! bug
	Docker your documentations hassle free.

!!! example
	Docker your documentations hassle free.

!!! quote
	Docker your documentations hassle free.

!!! unknown
	Fall back to default.
